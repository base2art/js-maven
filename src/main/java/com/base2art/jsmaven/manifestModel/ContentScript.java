package com.base2art.jsmaven.manifestModel;

import java.util.ArrayList;
import java.util.List;

public class ContentScript
{

  private final List<String> matches = new ArrayList<>();
  private final List<String> js = new ArrayList<>();
  private RunAtStyle run_at = RunAtStyle.document_idle;
  private boolean all_frames = true;

  public boolean isAll_frames()
  {
    return all_frames;
  }

  public void setAll_frames(boolean all_frames)
  {
    this.all_frames = all_frames;
  }

  public RunAtStyle getRun_at()
  {
    return run_at;
  }

  public void setRun_at(RunAtStyle run_at)
  {
    this.run_at = run_at;
  }

  public List<String> getJs()
  {
    return js;
  }

  public List<String> getMatches()
  {
    return matches;
  }
}

/*
 * {
 * "matches" : [ "http://trello.com/*", "https://trello.com/*" ], "js" : [ "b2a/com.b2a.DiskClassloader.js",
 * "b2a/com.b2a.System.js", "b2a/web/ui/com.b2a.web.ui.Selector.js", "b2a/net/com.b2a.net.WebConnection.js",
 * "b2a/lang/com.b2a.lang.String.js", "trello/com.b2a.trello.CardFormatter.js",
 * "trello/com.b2a.trello.BoardNavigator.js", "trello/com.b2a.trello.contentScript.js" ], "run_at" : "document_idle",
 * "all_frames" : false }
 */
