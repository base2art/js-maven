package com.base2art.jsmaven.manifestModel;

import java.util.ArrayList;
import java.util.List;

public class PageAction
{

  private String name = "Default Page Action";
  private final List<String> icons = new ArrayList<>();
  private String default_icon;
  private String default_title = "Default Page Action Title";

  public String getDefault_icon()
  {
    return default_icon;
  }

  public void setDefault_icon(String default_icon)
  {
    this.default_icon = default_icon;
  }

  public String getDefault_title()
  {
    return default_title;
  }

  public void setDefault_title(String default_title)
  {
    this.default_title = default_title;
  }

  public String getName()
  {
    return name;
  }

  public void setName(String name)
  {
    this.name = name;
  }

  public List<String> getIcons()
  {
    return icons;
  }
}
