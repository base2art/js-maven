package com.base2art.jsmaven.manifestModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Manifest
{

  private String name = "Sample Name";
  private String description = "Description";
  private String version = "0.0.1";
  private int manifest_version = 2;
  private IncognitoType incognito = IncognitoType.split;
  private final HashMap<Integer, String> icons = new HashMap<>();
  private final List<String> web_accessible_resources = new ArrayList<>();
  private final List<String> permissions = new ArrayList<>();
  private final List<ContentScript> content_scripts = new ArrayList<>();
  private PageAction page_action = new PageAction();

  public Manifest()
  {
    icons.put(16, "resources/main-icon-16.png");
    icons.put(48, "resources/main-icon-48.png");
    icons.put(128, "resources/main-icon-128.png");
  }

  public String getDescription()
  {
    return description;
  }

  public void setDescription(String description)
  {
    this.description = description;
  }

  public IncognitoType getIncognito()
  {
    return incognito;
  }

  public void setIncognito(IncognitoType incognito)
  {
    this.incognito = incognito;
  }

  public int getManifest_version()
  {
    return manifest_version;
  }

  public void setManifest_version(int manifest_version)
  {
    this.manifest_version = manifest_version;
  }

  public String getName()
  {
    return name;
  }

  public void setName(String name)
  {
    this.name = name;
  }

  public String getVersion()
  {
    return version;
  }

  public void setVersion(String version)
  {
    this.version = version;
  }

  public List<ContentScript> getContent_scripts()
  {
    return content_scripts;
  }

  public HashMap<Integer, String> getIcons()
  {
    return icons;
  }

  public PageAction getPage_action()
  {
    return page_action;
  }

  public void setPage_action(PageAction page_action)
  {
    this.page_action = page_action;
  }

  public List<String> getPermissions()
  {
    return permissions;
  }

  public List<String> getWeb_accessible_resources()
  {
    return web_accessible_resources;
  }
}

/*

  "page_action": {
    "name": "Make The Video the only thing on the page",
    "icons": ["resources/expand-video-48.png"],
    "default_icon": "resources/expand-video-48.png",
    "default_title": "Video Maximizer"
  },

  "content_scripts" : [
    
  ]
  
  */