package com.base2art.jsmaven;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import javax.script.ScriptEngine;
import javax.script.ScriptException;
import org.apache.commons.io.FileUtils;

public class ProjectModuleLoader
{

  private final EngineFactory factory;
  private final Project project;
  private List<Module> modules;
  private List<Module> specModules;

  public ProjectModuleLoader(EngineFactory factory, Project project)
  {
    this.factory = factory;
    this.project = project;

  }

  public List<Module> loadSpecFiles()
  {
    if (this.specModules == null)
    {
      this.specModules = this.getModules(this.project.getSpecFiles(), true);
    }

    return this.filter(this.specModules, new IPredicate<Module>()
    {

      @Override
      public boolean is(Module item)
      {
        return item.getName() == null;
      }
    });
  }

  public List<Module> loadFrameworkFiles()
  {
    if (this.modules == null)
    {
      this.modules = this.getModules(this.project.getSourceFiles(), false);
    }

    return this.filter(this.modules, new IPredicate<Module>()
    {

      @Override
      public boolean is(Module item)
      {
        return item.getName() == null;
      }
    });
  }

  public List<Module> loadModules()
  {
    if (this.modules == null)
    {
      this.modules = this.getModules(this.project.getSourceFiles(), false);
    }

    return this.filter(this.modules, new IPredicate<Module>()
    {

      @Override
      public boolean is(Module item)
      {
        return item.getName() != null;
      }
    });
  }

  private Module loadModule(File file) throws RuntimeException
  {
    try
    {
      String script = FileUtils.readFileToString(file);
      final ScriptEngine engine = this.factory.createEngine();

      engine.eval(
        this.getClassloader()
        + script
        + this.getClassUnloader());

      List<Map<String, ?>> list = (List<Map<String, ?>>) engine.get("$JAVA_BUILD_GLOBAL_MODULES$");
      List<String> lazyImports = (List<String>) engine.get("$JAVA_BUILD_LAZY_IMPORTS$");

      // isEmpty is implemented wrong you have to check size == 0
      // HOLY CRAP thats a wierd bug.
      if (list.size() == 0)
      {
//        System.out.println("No Defines found in: '" + file.getPath() + "'");
        return null;
      }

      assert (list.size() == 1);

      Map<String, ?> nativeObj = list.get(0);
      String name = (String) nativeObj.get("name");
      List<String> reqs = (List<String>) nativeObj.get("reqs");
      boolean isClass = (boolean)(Boolean) nativeObj.get("isClass");
      return new Module(
        name,
        reqs.toArray(new String[0]),
        lazyImports.toArray(new String[0]),
        file,
        isClass);
    }
    catch (IOException | ScriptException re)
    {
      System.out.println("Error Executing file: '" + file.getAbsolutePath() + "'");
      System.out.println(re.getMessage());
      throw new RuntimeException("ERROR COMPILING");
    }
  }

  private String getClassloader()
  {
    return ""
      + "window = {};"
      + "$JAVA_BUILD_GLOBAL_MODULES$ = [];"
      + "$JAVA_BUILD_LAZY_IMPORTS$ = [];"
      + "isFunc = function (objToCheck) { "
      + "if (objToCheck == null) { return false; }"
      + "var getType = {};"
      + "return objToCheck && getType.toString.call(objToCheck) === '[object Function]'; };"
      + "lazy_import = function(name){$JAVA_BUILD_LAZY_IMPORTS$.push(name);};"
      + "define = function(name, reqs, func){$JAVA_BUILD_GLOBAL_MODULES$.push({ name: name, reqs: reqs, isClass: isFunc(func) })};"
      + "require = function(reqs){return {};};";
  }

  private String getClassUnloader()
  {
    return "";
  }

  private List<Module> getModules(Collection<File> files, boolean skipLoading)
  {
    List<Module> tempModules = new ArrayList<>();
    for (File file : files)
    {
      if (skipLoading)
      {
        tempModules.add(new Module(null, new String[0], new String[0], file, false));
      }
      else
      {
        Module m = this.loadModule(file);
        if (m != null)
        {
          tempModules.add(m);
        }
        else
        {
          tempModules.add(new Module(null, new String[0], new String[0], file, false));
        }
      }
    }

    return tempModules;
  }

  private <T> List<T> filter(List<T> modules, IPredicate<T> predicate)
  {
    List<T> newItems = new ArrayList<>();
    for (int i = 0; i < modules.size(); i++)
    {
      T t = modules.get(i);
      if (predicate.is(t))
      {
        newItems.add(t);
      }

    }
    return newItems;
  }

  public File getProjectDirectory()
  {
    return this.project.getProjectDirectory();
  }

  public File getProjectSourceDirectory()
  {
    return this.project.getSourceDirectory();
  }
}
