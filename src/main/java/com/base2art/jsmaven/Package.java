package com.base2art.jsmaven;

import com.base2art.jsmaven.util.StartupUtil;

public class Package
{

  public static void main(String[] args) throws Exception
  {

    final EngineFactory engineFactory = new EngineFactory();
    Runner runner = new Runner(
      engineFactory,
      new ProjectModuleLoader(
      engineFactory,
      new Project(StartupUtil.projDir())));

    runner.compileForSpec();
    runner.compileForChromePlugin();
  }
}
