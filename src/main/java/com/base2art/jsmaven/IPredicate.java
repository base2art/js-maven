package com.base2art.jsmaven;


public interface IPredicate<T> {
  boolean is(T item);
}
