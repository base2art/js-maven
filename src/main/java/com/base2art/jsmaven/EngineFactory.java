package com.base2art.jsmaven;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;

public class EngineFactory
{

  private final ScriptEngineManager factory;

  public EngineFactory()
  {

    this.factory = new ScriptEngineManager();
  }

  public ScriptEngine createEngine()
  {
    return this.factory.getEngineByName("JavaScript");
  }
}
