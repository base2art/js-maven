package com.base2art.jsmaven;

import com.base2art.jsmaven.manifestModel.ContentScript;
import com.base2art.jsmaven.manifestModel.Manifest;
import com.base2art.jsmaven.util.DocumentUtils;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import javax.script.ScriptEngine;
import javax.script.ScriptException;
import org.apache.commons.io.FileUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class Runner
{

  private final EngineFactory factory;
  private final ProjectModuleLoader modLoader;

  public Runner(EngineFactory factory, ProjectModuleLoader modLoader)
  {
    this.factory = factory;
    this.modLoader = modLoader;
  }

  public void compileForSpec()
  {
    List<Module> frameworkFiles = modLoader.loadFrameworkFiles();
    List<Module> specFiles = modLoader.loadSpecFiles();
    ModuleDependencyResolver mdr = new ModuleDependencyResolver(modLoader.loadModules());

    List<Module> modules = mdr.resolveDependencyOrder();

    InputStream stream = this.getClass().getClassLoader().getResourceAsStream("SpecRunner.html");

    Document doc = DocumentUtils.read(stream);

    Element head = (Element) doc.getElementsByTagName("head").item(0);

//    System.out.println("Framework Files");
    for (int i = 0; i < frameworkFiles.size(); i++)
    {
      final Module mod = frameworkFiles.get(i);
      this.append(this.modLoader.getProjectDirectory(), head, mod);
//      System.out.println(mod.getFile());
    }

//    System.out.println("Module Files");
    for (int i = 0; i < modules.size(); i++)
    {
      final Module mod = modules.get(i);
      this.append(this.modLoader.getProjectDirectory(), head, mod);
//      System.out.println(mod.getFile());
    }

//    System.out.println("Spec Files");
    for (int i = 0; i < specFiles.size(); i++)
    {
      final Module mod = specFiles.get(i);
      this.append(this.modLoader.getProjectDirectory(), head, mod);
//      System.out.println(mod.getFile());
    }

    DocumentUtils.save(new File(this.modLoader.getProjectDirectory(), "SpecRunner.html"), doc);
  }

  public void compileForChromePlugin()
    throws ScriptException, IOException
  {
    try
    {
      Manifest manifest = new Manifest();
      ContentScript cs = new ContentScript();
      manifest.getContent_scripts().add(cs);
      manifest.setPage_action(null);


      List<Module> frameworkFiles = modLoader.loadFrameworkFiles();
      ModuleDependencyResolver mdr = new ModuleDependencyResolver(modLoader.loadModules());

      List<Module> modules = mdr.resolveDependencyOrder();

      for (Module module : frameworkFiles)
      {
        final String relative = this.relativize(this.modLoader.getProjectSourceDirectory(), module);
        cs.getJs().add(relative);
      }

      for (Module module : modules)
      {
        final String relative = this.relativize(this.modLoader.getProjectSourceDirectory(), module);

        if (!relative.contains("contentScript"))
        {
          cs.getJs().add(relative);
        }
      }

      for (Module module : modules)
      {
        final String relative = this.relativize(this.modLoader.getProjectSourceDirectory(), module);

        if (relative.contains("contentScript"))
        {
          cs.getJs().add(relative);
        }
      }

      ObjectMapper mapper = new ObjectMapper();
      mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
      final ObjectWriter writer = mapper.writerWithDefaultPrettyPrinter();
      writer.with(SerializationFeature.INDENT_OUTPUT);
      final File manifestFile = new File(this.modLoader.getProjectSourceDirectory(), "manifest.json");
      final File projectFile = new File(this.modLoader.getProjectDirectory(), "manifest.jsproj");
      if (projectFile.exists())
      {
        final ScriptEngine engine = this.factory.createEngine();
        engine.put("manifest", manifest);
        engine.eval(FileUtils.readFileToString(projectFile));
      }

      FileUtils.write(manifestFile, writer.writeValueAsString(manifest));

    }
    catch (JsonProcessingException ex)
    {
    }
  }

  private void append(File baseDir, Element head, Module module)
  {
    final Document owner = head.getOwnerDocument();
    head.appendChild(owner.createTextNode("\n\t"));
    String relative = relativize(baseDir, module);
    Element script = owner.createElement("script");
    script.setAttribute("type", "text/javascript");
    script.setAttribute("src", relative);

    head.appendChild(script);
    //type="text/javascript" src="lib/jasmine-2.0.0/jasmine.js"
  }

  private String relativize(File baseDir, Module module)
  {
    return baseDir.toURI().relativize(module.getFile().toURI()).getPath();
  }
}

/*

{
  "name": "Chromello",
  "description": "This extension hacks trello to have cooler more scrumish functionality.",
  "version": "0.0.1",
  "manifest_version": 2,
  "incognito": "split",

  "icons": {
    "16": "resources/expand-video-16.png",
    "48": "resources/expand-video-48.png",
    "128": "resources/expand-video-128.png"
  },

  "web_accessible_resources": ["resources/expand-video-16.png"],


  "permissions": [
    "https://trello.com/*",
    "tabs"
  ],

  "page_action": {
    "name": "Make The Video the only thing on the page",
    "icons": ["resources/expand-video-48.png"],
    "default_icon": "resources/expand-video-48.png",
    "default_title": "Video Maximizer"
  },

  "content_scripts" : [
    {
      "matches" : [
        "http://trello.com/*",
        "https://trello.com/*"
      ],
      "js" : [
        "b2a/com.b2a.DiskClassloader.js",
        "b2a/com.b2a.System.js",
        "b2a/web/ui/com.b2a.web.ui.Selector.js",
        "b2a/net/com.b2a.net.WebConnection.js",
        "b2a/lang/com.b2a.lang.String.js",
        "trello/com.b2a.trello.CardFormatter.js",
        "trello/com.b2a.trello.BoardNavigator.js",
        "trello/com.b2a.trello.contentScript.js"
      ],
      "run_at" : "document_idle",
      "all_frames" : false
    }
  ]
}


 */