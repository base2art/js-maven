package com.base2art.jsmaven;

import java.io.File;
import java.util.Collection;
import org.apache.commons.io.FileUtils;

public class Project
{

  private final File mainDirectory;

  public Project(File proj)
  {
    this.mainDirectory = proj;
  }

  public Collection<File> getSpecFiles()
  {
    File specDir = this.getSpecDirectory();
    return FileUtils.listFiles(specDir, new String[]
      {
        "js"
      }, true);
  }

  public Collection<File> getSourceFiles()
  {
    File srcDir = this.getSourceDirectory();
    return FileUtils.listFiles(srcDir, new String[]
      {
        "js"
      }, true);
  }

  public File getProjectDirectory()
  {
    return this.mainDirectory;
  }

  public File getSourceDirectory()
  {
    return new File(this.mainDirectory, "src");
  }

  public File getSpecDirectory()
  {
    return new File(this.mainDirectory, "spec");
  }
}
