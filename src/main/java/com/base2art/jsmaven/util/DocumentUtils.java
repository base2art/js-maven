package com.base2art.jsmaven.util;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

public class DocumentUtils
{

  public static Document read(InputStream stream)
  {
    try
    {
      DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
      DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
      return dBuilder.parse(stream);
    }
    catch (ParserConfigurationException | SAXException | IOException ex)
    {
      return null;
    }
  }

  public static void save(File file, Document doc)
  {
    try
    {
      TransformerFactory transformerFactory = TransformerFactory.newInstance();
      Transformer transformer = transformerFactory.newTransformer();
      DOMSource source = new DOMSource(doc);
      StreamResult result = new StreamResult(file);

      // Output to console for testing
      // StreamResult result = new StreamResult(System.out);

      transformer.transform(source, result);
      

//      result.getOutputStream().flush();
    }
    catch (TransformerException ex)
    {
    }
  }
}
