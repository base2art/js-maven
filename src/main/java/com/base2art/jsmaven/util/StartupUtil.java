package com.base2art.jsmaven.util;

import java.io.File;

public class StartupUtil
{

  public static File projDir()
  {
    final String currDirectory = System.getProperty("user.dir");

    File workingDir = getDirectory(new File(currDirectory), 0, 4);

    if (workingDir != null && !new File(workingDir, "pom.xml").exists())
    {
      return workingDir;
    }

    return new File("/home/tyoung/code/base2art/trello-extensions");
  }

  private static File getDirectory(File curr, int i, int max)
  {
    if (new File(curr, "src").exists())
    {
      return curr;
    }

    if (i > max)
    {
      return null;
    }

    return getDirectory(curr.getParentFile(), i + 1, max);
  }
}
