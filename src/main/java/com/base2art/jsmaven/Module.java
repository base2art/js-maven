package com.base2art.jsmaven;

import java.io.File;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import org.apache.commons.lang.StringUtils;

public class Module
{

  private final String name;
  private final String[] dependencies;
  private final String[] lazyImports;
  private final File file;
  private final boolean isClass;

  public Module(
    String name,
    String[] dependencies,
    String[] lazyImports,
    File file,
    boolean isClass)
  {
    this.name = name;
    this.dependencies = Arrays.<String>copyOf(dependencies, dependencies.length);
    this.lazyImports = lazyImports;
    this.file = file;
    this.isClass = isClass;
  }

  public boolean isFrameworkFile()
  {
    return name == null || name.isEmpty();
  }

  public String[] getDependencies()
  {
    return dependencies;
  }

  public String[] getLazyImports()
  {
    return lazyImports;
  }

  public String[] getEagerImports()
  {
    Set<String> set = new HashSet<>();
    set.addAll(Arrays.asList(this.dependencies));

    set.removeAll(Arrays.asList(this.lazyImports));

    return set.toArray(new String[0]);
  }

  public File getFile()
  {
    return file;
  }

  public String getName()
  {
    return name;
  }

  public boolean isIsClass()
  {
    return isClass;
  }

  @Override
  public String toString()
  {

    return String.format(
      "%s : %s",
      this.name,
      StringUtils.join(this.dependencies, ", "));
  }
}
