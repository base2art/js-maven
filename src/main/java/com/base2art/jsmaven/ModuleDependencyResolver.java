package com.base2art.jsmaven;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import org.apache.commons.lang.ArrayUtils;

public class ModuleDependencyResolver
{

  private final List<Module> modules;

  public ModuleDependencyResolver(List<Module> modules)
  {
    this.modules = new ArrayList<>(modules);
  }

  public List<Module> resolveDependencyOrder()
  {
    List<Module> queue = new LinkedList<>();

    this.resolve(new ArrayList<>(this.modules), queue);

//    HashMap<String, List<Module>> map = this.getMap();

//    for (String moduleName : map.keySet())
//    {
//      List<Module> referencingModule = map.get(moduleName);
//      if (referencingModule.isEmpty())
//      {
//        Module module = this.getModuleByName(moduleName);
//        this.addItem(map, module, queue);
//      }
//    }

    return queue;
  }

  private HashMap<String, List<Module>> getMap()
  {
    HashMap<String, List<Module>> map = new HashMap<>();

    for (Module module : modules)
    {
      List<Module> mods = new ArrayList<>();
      final String modName = module.getName();
      map.put(modName, mods);

      for (Module tempModule : modules)
      {
        for (String dep : tempModule.getEagerImports())
        {
          if (dep.equals(modName))
          {
            mods.add(tempModule);
          }
        }
      }
    }

    return map;
  }

  private Module getModuleByName(String moduleName)
  {
    for (Module module : this.modules)
    {
      if (module.getName().equals(moduleName))
      {
        return module;
      }
    }

    return null;
  }

  private boolean contains(String[] dependencies, String name)
  {
    return ArrayUtils.contains(dependencies, name);
  }

  private boolean contains(List<Module> queue, Module module)
  {
    return this.contains(queue, module.getName());
  }

  private boolean contains(List<Module> queue, String moduleName)
  {
    for (int i = 0; i < queue.size(); i++)
    {
      Module module1 = queue.get(i);
      if (module1.getName().equals(moduleName))
      {
        return true;
      }
    }

    return false;
  }

  private void resolve(List<Module> remaining, List<Module> queue)
  {
    for (Module module : remaining)
    {
      if (this.contains(queue, module))
      {
        continue;
      }

      final String[] eagerImports = module.getEagerImports();
      boolean hasAll = true;
      for (int i = 0; i < eagerImports.length; i++)
      {
        String eagerImport = eagerImports[i];
        hasAll &= this.contains(queue, eagerImport);
      }

      if (hasAll)
      {
        this.addItem(module, remaining, queue);
        this.resolve(remaining, queue);
        return;
      }
    }
  }

  private void addItem(Module module, List<Module> remaining, List<Module> queue)
  {
    final String moduleName = module.getName();
    for (int i = 0; i < queue.size(); i++)
    {
      Module module1 = queue.get(i);
      if (module1.getName().equals(moduleName))
      {
        remaining.remove(i);
        break;
      }
    }

    queue.add(module);
  }
}


/*

  private void addItem(
    HashMap<String, List<Module>> map,
    Module module,
    List<Module> queue)
  {
    int index;

    if (this.contains(queue, module))
    {
      return;
    }

    for (index = 0; index < queue.size(); index++)
    {
      Module targets = queue.get(index);
      if (this.contains(targets.getDependencies(), module.getName()))
      {
        break;
      }
    }

    queue.add(index, module);
    final String[] dependencies = module.getDependencies();
    for (int i = 0; i < dependencies.length; i++)
    {
      String moduleName = dependencies[i];
      Module subModule = this.getModuleByName(moduleName);
      this.addItem(map, subModule, queue);
    }
  }*/